# README #

![3644850654-7-peregrine-logo_avatar.png](https://bitbucket.org/repo/appEMA/images/2511827325-3644850654-7-peregrine-logo_avatar.png)

### What is Peregrine_IRC? ###
Peregrine is a simple bot for IRC.
This bot is designed to post Twitter Tweets from a feed right into your IRC channel. 
It is designed to work with twitter users how ever it can work with keywords or more ( with a little tinkering ).

Feel free to fork this. I hope to make other bots. I will try to keep them separate that way people can choose what they need.
Bots can always be merged together to perform several tasks. 

### What is this repository for? ###

* Peregrine_IRC Bot for IRC. Tweets to IRC channel.
* 1.0.0

### How do I get set up? ###

* Clone Repo
* Edit ConfigFile_EXAMPLE.json and add credentials. Then save it as ConfigFile.json
* apt get install node.js or install .exe
* npm install (Dependencies) IRC, twitter, multiline
* node Peregrine_IRC.js or Start_Peregrine_IRC.bat

### Contribution guidelines ###

* Fork
* Fork
* Fork

### Who do I talk to? ###

* Peter Stevenson (2E0PGS)

### Licence? ###

```
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```


##### Photo source? #####
*Peregrine Falcon
https://www.flickr.com/photos/66164549@N00/2314217251
By: Keven Law*