/*
Peregrine_IRC a basic bot with the core function being twitter to IRC

Copyright (C) <2015>  <Peter Stevenson (2E0PGS)>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var irc = require('irc'); //Core IRC library
var multiLine = require("multiline"); //used so we can have nice multiLine /help responce
var configFile = require("./ConfigFile.json"); //configFile stores configuration and login credentials.
var twitter = require('twitter'); //required to interface with twitter
var twitterUrl = "https://twitter.com/2E0PGSWX";
var ircChannel = "#NBARC"; //set IRC channel here

var client = new irc.Client('chat.freenode.net', 'Peregrine', { //join NBARC IRC channel
	channels: [(ircChannel)], realName: '2E0PGS Bot', port: 6697, autoRejoin: true, autoConnect: true, secure: true, floodProtection: true, floodProtectionDelay: 500,
});

client.addListener('message', function (from, to, message) {
	console.log(from + ' => ' + to + ': ' + message);
	if (message === "!help") { //Here is our command list. Add commands here.
		client.say((ircChannel), (help));
	}
	if (message === "!about") {
		client.say((ircChannel), (about));
	}
	if (message === "!commands") {
		client.say((ircChannel), (help));
	}
	if (message === "!twitter") {
		client.say((ircChannel), (twitterUrl));
	}
	if (message === "!ping") {
		client.say((ircChannel), "Pong!");
	}
});

client.addListener('error', function (message) { //log errors to prevent fatal crash.
    console.log('error: ', message);
    process.exit(1); //exit node.js with an error
});


console.log("twitter Tracking Enabled!"); //connect to twitter.
var twitterClient = new twitter({
	consumer_key: configFile.twitter_consumer_key,
	consumer_secret: configFile.twitter_consumer_secret,
	access_token_key: configFile.twitter_access_token_key,
	access_token_secret: configFile.twitter_access_token_secret,
});

//------------------------twitter_Tracking------------------------
twitterClient.stream('statuses/filter', {follow: (configFile.twitter_account_to_follow)},  function(stream){
	stream.on('data', function(tweet) { //stream with new tweet
		console.log("New tweet from tracker: " + tweet.text);
		client.say((ircChannel), (tweet.text));
	});

	stream.on('error', function(error) { //stream with error
		console.log(error);
		console.log("twitter Disconnected!");
		process.exit(1); //exit node.js with an error
	});

});
//------------------------twitter_Tracking---------------------END




//Command strings
var help = multiLine(function () {/*
##############################################
Help function:

!ping Checks if bot is online.

!help Lists avalible commands.

!twitter Prints link to tracked twitter account.

!about Prints about message.
##############################################
*/});

//About string
var about = multiLine(function () {/*
########################################################
A simple bot with a few core functions.
Main function is to post twitter feed into channel chat.
Owner: @2E0PGS
Written by: @2E0PGS
########################################################
*/});
