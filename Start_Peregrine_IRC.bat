@echo off

REM Peregrine_IRC .bat start script for node.js program

title Peregrine_IRC

color e

@echo Starting Peregrine_IRC...

node Peregrine_IRC.js

pause
